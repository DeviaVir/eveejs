var blogId = '93db438315cafa132e4903ef925bd997'
  , posts
  , app = Sammy('#main', function() {

  this.get('#!/about', function() {
    $('.page').hide();$('.about').show();
  });

  this.get('#!/contact', function() {
    $('.page').hide();$('.contact').show();
  });

  this.get('#!/post/:id', function() {
    $('.page').hide();$('.post').show().children('article').remove();
    var loader = $('<span />').addClass( 'icon-spin icon-spinner' ).appendTo( $( '.post' ) )
    load('http://evee.cc/v1/post/' + this.params['id'], function(xhr) {
      loader.remove();
      post = JSON.parse( xhr.responseText )
      var layout = {
        title: post.title,
        content: post.content,
        user: post.user,
        date: post.date,
        humanDate: null
      }

      var date  = new Date( layout.date * 1000 )
        , human = ( (date.getMonth()+1) < 10 ? '0' : '' ) + (date.getMonth()+1) + '/' + ( date.getDate() < 10 ? '0' : '' ) + date.getDate() + '/' + date.getFullYear() + ' ' + ( date.getHours() < 10 ? '0' : '' ) + date.getHours() + ':' + ( date.getMinutes() < 10 ? '0' : '' ) + date.getMinutes()
        , content = layout.content
      layout.humanDate = human;
      content = content.split( '<br>' ).join( '\n\n' );
      content = content.replace(/(<([^>]+)>)/ig,"")
      console.log( content )

      $('.post').append(
        $('<article />').append(
          $('<h3 />').append(
            $('<a />').attr( 'href', '#!/post/' + layout.id ).text( layout.title )
          )
        ).append(
          $('<h6 />').text( 'Posted on ' + layout.humanDate + ' by ' + post.user )
        ).append(
          $('<div />').html( mmd( content ) )
        )
      )

    })
  })

  this.get('#!/home', function() {
    $('.page').hide();$('.home').show().children().remove();
    var loader = $('<span />').addClass( 'icon-spin icon-spinner' ).appendTo( $( '.page' ) )
    load('http://evee.cc/v1/posts/' + blogId, function(xhr) {
        loader.remove(); posts = JSON.parse( xhr.responseText );
        $.each( posts.data, function(i,v) {
          var layout = {
            title: v.value.title,
            content: v.value.content,
            id: v.id,
            published: v.value.published,
            date: v.value.date,
            humanDate: null
          }

          if( layout.published ) {
            var date  = new Date( layout.date * 1000 )
              , human = ( (date.getMonth()+1) < 10 ? '0' : '' ) + (date.getMonth()+1) + '/' + ( date.getDate() < 10 ? '0' : '' ) + date.getDate() + '/' + date.getFullYear() + ' ' + ( date.getHours() < 10 ? '0' : '' ) + date.getHours() + ':' + ( date.getMinutes() < 10 ? '0' : '' ) + date.getMinutes()
              , content = layout.content
            layout.humanDate = human;
            content = content.split( '<br>' ).join( '\n\n' );
            content = content.replace(/(<([^>]+)>)/ig,"")

            $('.home').append(
              $('<article />').append(
                $('<h3 />').append(
                  $('<a />').attr( 'href', '#!/post/' + layout.id ).text( layout.title )
                ).append(
                  $('<a />').addClass('button right tiny radius').attr( 'href', '#!/post/' + layout.id ).html('Permalink &rarr;')
                )
              ).append(
                $('<h6 />').text( 'Posted on ' + layout.humanDate )
              ).append(
                $('<div />').html( mmd( content ) )
              )
            )
          }

        })
    });
  });
});

;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  $(document).ready(function() {
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationButtons          ? $doc.foundationButtons() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
    $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
    $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
    $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
    $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
    $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
    $.fn.foundationClearing         ? $doc.foundationClearing() : null;

    $.fn.placeholder                ? $('input, textarea').placeholder() : null;

    app.run('#!/home');
  });

  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }

})(jQuery, this);

function load(url, callback) {
    var xhr;
    if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
    else {
        var versions = ["MSXML2.XmlHttp.5.0",
                        "MSXML2.XmlHttp.4.0",
                        "MSXML2.XmlHttp.3.0",
                        "MSXML2.XmlHttp.2.0",
                        "Microsoft.XmlHttp"]
         for(var i = 0, len = versions.length; i < len; i++) {
            try {
                xhr = new ActiveXObject(versions[i]);
                break;
            }
            catch(e){}
         } // end for
    }
    xhr.onreadystatechange = ensureReadiness;
    function ensureReadiness() {
        if(xhr.readyState < 4) {
            return;
        }
        if(xhr.status !== 200) {
            return;
        }
        // all is well
        if(xhr.readyState === 4) {
            callback(xhr);
        }
    }
    xhr.open('GET', url, true);
    xhr.send('');
}